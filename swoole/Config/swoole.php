<?php
/**
 * @see https://github.com/hhxsv5/laravel-s/blob/master/Settings-CN.md  Chinese
 * @see https://github.com/hhxsv5/laravel-s/blob/master/Settings.md  English
 */
return [
    'listen_ip'                => '127.0.0.1',
    'listen_port'              => 5200,
    'socket_type'              => defined('SWOOLE_SOCK_TCP') ? \SWOOLE_SOCK_TCP : 1,
    'enable_coroutine_runtime' => false,
    'server'                   => 'TPSwoole',
    'inotify_reload'           => [
        'enable'        => true,
        'watch_path'    => '',
        'file_types'    => ['.php'],
        'excluded_dirs' => [],
        'log'           => true,
    ],
    'event_handlers'           => [
    ],
    'websocket'                => [
        'enable' => false,
    ],
    'sockets'                  => [
    ],
    'processes'                => [
    ],
    'timer'                    => [
        'enable' => false,
        'jobs'   => [
        ],
    ],
    'events'                   => [
    ],
    'swoole_tables'            => [
    ],
    'register_providers'       => [
    ],
    'swoole'                   => [
        'daemonize'          => true,
        'dispatch_mode'      => 2,
        'reactor_num'        => function_exists('\swoole_cpu_num') ? \swoole_cpu_num() * 2 : 4,
        'worker_num'         => 10,
        'task_ipc_mode'      => 1,
        'task_max_request'   => 5000,
        'max_request'        => 3000,
        'open_tcp_nodelay'   => true,
        'pid_file'           => APP_PATH . 'application/swoole/storage/swoole.pid',
        'log_file'           => '',
        'log_level'          => 4,
        'document_root'      => '',
        'buffer_output_size' => 2 * 1024 * 1024,
        'socket_buffer_size' => 128 * 1024 * 1024,
        'package_max_length' => 4 * 1024 * 1024,
        'reload_async'       => true,
        'max_wait_time'      => 60,
        'enable_reuse_port'  => true,
        'enable_coroutine'   => false,
        'http_compression'   => false,
    ],
];
