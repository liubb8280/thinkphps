<?php
namespace app\swoole\src;

class ThinksCore
{
    private $swoole;

    public function start()
    {
        $serverClass = \swoole_websocket_server::class;
        // 2. 执行应用
        $this->swoole = new $serverClass('127.0.0.1', 50001, \SWOOLE_PROCESS, \SWOOLE_SOCK_TCP);

        $settings = [
//            'daemonize'          => true,
            'dispatch_mode'      => 2,
            'reactor_num'        => function_exists('\swoole_cpu_num') ? \swoole_cpu_num() * 2 : 4,
            'worker_num'         => 20,
            'task_ipc_mode'      => 1,
            'task_max_request'   => 5000,
            'max_request'        => 3000,
            'open_tcp_nodelay'   => true,
            'pid_file'           => '/home/wwwlogs/swoole.pid',
            'log_file'           => '/home/wwwlogs/swoole.log',
            'request_slowlog_file' => '/home/wwwlogs/swoole_slow.log',
            'log_level'          => 3,
            'document_root'      => '',
            'buffer_output_size' => 2 * 1024 * 1024,
            'socket_buffer_size' => 128 * 1024 * 1024,
            'package_max_length' => 4 * 1024 * 1024,
            'reload_async'       => true,
            'max_wait_time'      => 60,
            'enable_reuse_port'  => true,
            'enable_coroutine'   => false,
            'http_compression'   => false,
        ];
        $this->swoole->set($settings);

        $this->swoole->on('Start', [new self(), 'onStart']);
        $this->swoole->on('Shutdown', [$this, 'onShutdown']);
        $this->swoole->on('ManagerStart', [$this, 'onManagerStart']);
        $this->swoole->on('ManagerStop', [$this, 'onManagerStop']);
        $this->swoole->on('WorkerStart', [$this, 'onWorkerStart']);
        $this->swoole->on('WorkerStop', [$this, 'onWorkerStop']);
        $this->swoole->on('WorkerError', [$this, 'onWorkerError']);
        $this->swoole->on('PipeMessage', [$this, 'onPipeMessage']);
        $this->swoole->on('Request', [$this, 'onRequest']);
        $this->swoole->on('Message', [$this, 'onMessage']);

        $this->swoole->start();
    }

    public function onStart()
    {
        echo 123;
    }

    public function onShutdown()
    {
        echo "shutdown";
    }

    public function onManagerStart()
    {
        echo "managerStart";
    }

    public function onManagerStop()
    {
        echo "managerStop";
    }

    public function onWorkerStart()
    {
        \app\swoole\Console\App::workStart();
    }

    public function onWorkerStop()
    {
        echo "onWorkerStop";
    }

    public function onWorkerError()
    {
        echo "onWorkerError";
    }

    public function onPipeMessage()
    {
        echo "onPipeMessage";
    }

    public function onMessage(\swoole_websocket_server $server, \swoole_websocket_frame $frame)
    {
        echo "onMessage";
    }

    public function onRequest($request, $response)
    {
        try {
            $_GET = $request->get;
            $_POST = $request->post;
            $_SERVER = $request->server;

            $result = \app\swoole\Console\App::receive();
            $header = $result['header'];
            $content = $result['content'];
            foreach ($header as $key => $value) {
                $response->header($key, $value);
            }
            $response->end($content);
        } catch (\Exception $e) {
            error_log($e->getMessage());
            $response->end($e->getMessage() . ':' . $e->getFile() . ':' . $e->getLine());
        } catch (\Throwable $e) {
            error_log($e->getMessage());
            $response->end($e->getMessage() . ':' . $e->getFile() . ':' . $e->getLine());
        }
        return;
    }

}